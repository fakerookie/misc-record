fn main()
{
    let add_ = |x| {
        let res = move |y| x + y;
        res
    };
    
    let add5 = add_(5);
    println!("0 plus 5 equal {}", add5(0));

    /* Another Way, Wirte this directly.
    println!("0 plus 5 equal {}", add_(0)(5));
    */
}

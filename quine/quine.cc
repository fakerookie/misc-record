#include <iostream>

auto const QUINE = R"***(
// QUINE will be above this line

auto const BEFORE_QUINE =
    "#include <iostream>\n\n"
    "auto const QUINE = R\"***(";
auto const AFTER_QUINE = ")***\";";

int main()
{
    std::cout << BEFORE_QUINE << QUINE << AFTER_QUINE << QUINE;
}
)***";
// QUINE will be above this line

auto const BEFORE_QUINE =
    "#include <iostream>\n\n"
    "auto const QUINE = R\"***(";
auto const AFTER_QUINE = ")***\";";

int main()
{
    std::cout << BEFORE_QUINE << QUINE << AFTER_QUINE << QUINE;
}
#!/usr/bin/env python

# Version One
_='_=%r;print (_%%_)';print (_%_)

# Version Two
print(open(__file__).read())

# Version Three
q='q=%r;print (q%%q)';print (q%q)

# Version Four
var = "print('var = ', repr(var), '\\neval(var)')"
eval(var)

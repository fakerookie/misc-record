#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int add1(int n)
{
        return n + 1;
}

int sub1(int n)
{
        return n - 1;
}

int* getFunc()
{
        int(*Funcs[2])(int) = {add1, sub1};
        return Funcs[rand() % 2];
}

void apply(int f(int), int arg)
{
        int ret = (*f)(arg);
        printf("%d\n", arg);
}

int main()
{
        srand(time(NULL));
    
        for (int i = 0; i < 5; i++) {
            apply(getFunc(), 8);
        }
    
        return 0;
}